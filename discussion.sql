--  [Section] Advance selects

-- Exclude records or Not operator
SELECT FROM songs WHERE id != 3;

-- Greater than or equal;
SELECT * FROM songs where id > 3;
SELECT * FROM songs where id >= 3;

-- Less than or equal;
SELECT * FROM songs where id < 11;
SELECT * FROM songs where id <= 11;

-- In operator
SELECT * FROM songs WHERE id IN (1,3,11);

-- Partial matches
SELECT * FROM songs WHERE song_name LIKE "%a";
SELECT * FROM songs WHERE song_name LIKE "a%";
SELECT * FROM songs WHERE song_name LIKE "%_a_";

-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct record
SELECT DISTINCT genre FROM songs;

-- [INNER JOIN]
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;
SELECT albums.album_title,songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;

-- [LEFT JOIN] 
SELECT * FROM albums LEFT JOIN songs ON albums.id = songs.album_id;
SELECT albums.album_title,songs.song_name FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- [JOIN MULTIPLE TABLES]
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- [RIGHT JOIN]
SELECT * FROM albums RIGHT JOIN songs ON albums.id = songs.album_id;

-- [OUTER JOIN]
SELECT * FROM albums LEFT OUTER JOIN songs ON albums.id = songs.album_id
	UNION
	SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.album_id;