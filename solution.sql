-- Find all artists that have the letter d in their name.

SELECT * FROM artists WHERE name LIKE "%d%";

-- Find all songs that have a length of less than 230.

SELECT * FROM songs WHERE length < 230;

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)

SELECT albums.album_title, songs.song_name, songs.length 
	FROM albums JOIN songs ON albums.id = songs.album_id;

-- Join the 'artists' and 'albums' tables. (Find all albums that have the letter a in their name.)

SELECT artists.name, albums.album_title 
	FROM artists JOIN albums ON artists.id = albums.artist_id 
	WHERE albums.album_title LIKE "%a%";

-- Sort the albums in Z-A order. (Show only the first 4 records.)

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)

SELECT albums.album_title, songs.song_name, songs.length 
	FROM albums JOIN songs ON albums.id = songs.album_id 
	ORDER BY albums.album_title DESC;